# Elika.EntityFrameworkCore

### Установка

#### Паттерн `Repository`

Первоначально для работы с БД требуется создание класса - контекста базы данных.

```csharp
namespace Demo.Data
{
    public class DemoDbContext : EfDbContextRepository
    {
        public DemoDbContext(EntityCollection entityCollection) : base(entityCollection)
        {
        }

        public DemoDbContext(EntityCollection entityCollection, DbContextOptions options) : base(entityCollection, options)
        {
        }
    }
}
```

Далее требуется создать сущность БД - данная сушность является таблицей.
Сущности могут быть 2 видов:
- `IEntity` - сущность имеющая уже реализованное ключевое свойство
- `IDomain` - базовая основа сущности без дополнительных свойств


```csharp
namespace Demo.Domains
{
    public class UserDomain : IEntity
    {
        public int Id { get; set; }

        public int Name { get; set; }
    }
}
```

Далее к данной сушности требуется создать конфигурацию - маппинг таблицы.

Интерфейсом для конфигурации является дополнительная реализация штатного интерфейса `IEntityTypeConfiguration` из `EFCore`, дополнительная реализация включает в себя указания типа контекста БД к которому относится данная сущность.

```csharp
namespace Demo.Mappings
{
    public class UserMap : IEntityTypeConfiguration<DemoDbContext, UserDomain>
    {
        public void Configure(EntityTypeBuilder<UserDomain> builder)
        {
            builder.HasKey(c => c.Id);
            builder.ToTable("Users");
        }
    }
}
```

Для использования данных расширений требуется подключить как сам контекст используя расширение `AddElikaDbContext`, а так же автоматический поиск конфигурации сущностей в сборках используя расширение `AddAutoLoadMappings` (в противном случае требуется самим регистрировать конфигурации сущностей).

```csharp
namespace Demo
{
    public class Startup
    {
        ...
        public void ConfigureServices(IServiceCollection services)
        {
            ...
            services.AddElikaDbContext<DemoDbContext>(options => options.UseSqlite("Filename=demo.db"));
            services.AddAutoLoadMappings();
            ...
        }
        ...
    }
}
```
