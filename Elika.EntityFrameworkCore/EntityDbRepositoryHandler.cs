﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Elika.Data;
using Elika.Data.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Elika.EntityFrameworkCore
{
    /// <inheritdoc/>
    internal class EntityDbRepositoryHandler<TDomain> : IDbRepositoryHandler<TDomain> where TDomain : class, IDomain
    {
        private readonly DbContext _context;
        private readonly DbSet<TDomain> _dbSet;

        /// <inheritdoc/>
        public EntityDbRepositoryHandler(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TDomain>();
        }

        /// <inheritdoc/>
        public IQueryable<TDomain> GetTableQueryable()
        {
            return _dbSet.AsQueryable();
        }

        /// <summary>
        ///     Получить локальную таблицу
        /// </summary>
        /// <returns>Локальная таблица</returns>
        public IQueryable<TDomain> GetLocalTableQueryable()
        {
            return _dbSet.Local.AsQueryable();
        }

        /// <inheritdoc/>
        public int AddHandler(TDomain domain)
        {
            _dbSet.Add(domain);
            return _context.SaveChanges();
        }

        /// <inheritdoc/>
        public int AddRangeHandler(TDomain[] domains)
        {
            _dbSet.AddRange(domains);
            return _context.SaveChanges();
        }

        /// <inheritdoc/>
        public int UpdateHandler(TDomain domain)
        {
            _dbSet.Update(domain);
            return _context.SaveChanges();
        }

        /// <inheritdoc/>
        public int UpdateRangeHandler(TDomain[] domains)
        {
            _dbSet.UpdateRange(domains);
            return _context.SaveChanges();
        }

        /// <inheritdoc/>
        public int DeleteHandler(TDomain domain)
        {
            _dbSet.Remove(domain);
            return _context.SaveChanges();
        }

        /// <inheritdoc/>
        public int DeleteRangeHandler(TDomain[] domains)
        {
            _dbSet.RemoveRange(domains);
            return _context.SaveChanges();
        }

        /// <inheritdoc/>
        public async Task<int> AddAsyncHandler(TDomain domain, CancellationToken cancellationToken)
        {
            await _dbSet.AddAsync(domain, cancellationToken);
            return await _context.SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<int> AddRangeAsyncHandler(TDomain[] domains, CancellationToken cancellationToken)
        {
            await _dbSet.AddRangeAsync(domains, cancellationToken);
            return await _context.SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<int> UpdateAsyncHandler(TDomain domain, CancellationToken cancellationToken)
        {
            _dbSet.Update(domain);
            return await _context.SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<int> UpdateRangeAsyncHandler(TDomain[] domains, CancellationToken cancellationToken)
        {
            _dbSet.UpdateRange(domains);
            return await _context.SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<int> DeleteAsyncHandler(TDomain domain, CancellationToken cancellationToken)
        {
            _dbSet.Remove(domain);
            return await _context.SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<int> DeleteRangeAsyncHandler(TDomain[] domains, CancellationToken cancellationToken)
        {
            _dbSet.RemoveRange(domains);
            return await _context.SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc/>
        public int TruncateHandler()
        {
            var tableName = _context.Model.FindEntityType<TDomain>().GetTableName();
            var sql = tableName.Contains(" ") ? $"TRUNCATE TABLE [{tableName}]" : $"TRUNCATE TABLE {tableName}";
            return _context.Database.ExecuteSqlRaw(sql);
        }

        /// <inheritdoc/>
        public Task<int> TruncateAsyncHandler(CancellationToken cancellationToken = default)
        {
            var tableName = _context.Model.FindEntityType<TDomain>().GetTableName();
            var sql = tableName.Contains(" ") ? $"TRUNCATE TABLE [{tableName}]" : $"TRUNCATE TABLE {tableName}";
            return _context.Database.ExecuteSqlRawAsync(sql);
        }
    }
}
