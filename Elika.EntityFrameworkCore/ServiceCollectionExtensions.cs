﻿using System;
using System.Reflection;
using Elika.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Elika.EntityFrameworkCore
{
    /// <summary>
    ///     Расширения для <see cref="IServiceCollection"/>
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        ///     Добавить контекст базы данных
        /// </summary>
        /// <typeparam name="TDbContext">Тип контекста</typeparam>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="optionAction">Параметры контекста</param>
        /// <param name="autoLoadMappings">Флаг использования автоматической загрузки мапингов сущностей</param>
        /// <param name="assemblies">Список сборок из которых получать мапинги сущностей</param>
        /// <returns>Коллекция сервисов</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddElikaDbContext<TDbContext>(this IServiceCollection services, Action<DbContextOptionsBuilder> optionAction = null, bool autoLoadMappings = true, Func<Assembly[]> assemblies = null) where TDbContext : DbContext, IDbContext
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            optionAction ??= builder => { }; 

            MappingValidator.Register(typeof(TDbContext), EfMappingValidator.Validator);

            return services
                .RegisterDbContextAsRepository<TDbContext>(autoLoadMappings, assemblies)
                .AddDbContext<TDbContext>(optionAction);
        }

        /// <summary>
        ///     Добавить пул контекста базы данных
        /// </summary>
        /// <typeparam name="TDbContext">Тип контекста</typeparam>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="optionAction">Параметры контекста</param>
        /// <param name="poolSize">Размер пула</param>
        /// <param name="autoLoadMappings">Флаг использования автоматической загрузки мапингов сущностей</param>
        /// <param name="assemblies">Список сборок из которых получать мапинги сущностей</param>
        /// <returns>Коллекция сервисов</returns>
        public static IServiceCollection AddElikaDbContextPool<TDbContext>(this IServiceCollection services, Action<DbContextOptionsBuilder> optionAction = null, int poolSize = 128, bool autoLoadMappings = true, Func<Assembly[]> assemblies = null) where TDbContext : DbContext, IDbContext
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            optionAction ??= builder => { };

            MappingValidator.Register(typeof(TDbContext), EfMappingValidator.Validator);

            return services
                .RegisterDbContextAsRepository<TDbContext>(autoLoadMappings, assemblies)
                .AddDbContextPool<TDbContext>(optionAction, poolSize);
        }
    }
}
