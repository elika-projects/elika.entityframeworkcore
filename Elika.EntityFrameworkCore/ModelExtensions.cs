﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Elika.EntityFrameworkCore
{
    /// <summary>
    ///     Расширения для <see cref="IModel"/>
    /// </summary>
    public static class ModelExtensions
    {
        /// <summary>
        ///     Получить маппинг сущности наданных по типу <typeparamref name="TEntity"/>
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="model">Метаданные о сущностях</param>
        /// <returns>Маппинг сущности</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IEntityType FindEntityType<TEntity>(this IModel model) where TEntity : class
        {
            if(model == null)
                throw new ArgumentNullException(nameof(model));

            return model.FindEntityType(typeof(TEntity));
        }
    }
}
