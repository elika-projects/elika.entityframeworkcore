﻿using System;
using System.Threading.Tasks;
using Elika.Data;
using Microsoft.EntityFrameworkCore;

namespace Elika.EntityFrameworkCore
{
    /// <summary>
    ///     Контекст базы данных
    /// </summary>
    public abstract class EfDbContext : DbContext, IDbContext
    {
        private readonly EntityCollection _entityCollection;

        /// <summary>
        ///     Инициализация нового объекта типа <see cref="EfDbContext"/> с указанием коллекции сущностей
        /// </summary>
        /// <param name="entityCollection">Коллекция сущностей</param>
        public EfDbContext(EntityCollection entityCollection)
        {
            _entityCollection = entityCollection ?? throw new ArgumentNullException(nameof(entityCollection));
        }

        /// <summary>
        ///     Инициализация нового объекта типа <see cref="EfDbContext"/> c с указанием коллекции сущностей и опций
        /// </summary>
        /// <param name="entityCollection">Коллекция сущностей</param>
        /// <param name="options">Опции</param>
        public EfDbContext(EntityCollection entityCollection, DbContextOptions options) : base(options)
        {
            _entityCollection = entityCollection ?? throw new ArgumentNullException(nameof(entityCollection));
        }

        /// <summary>
        ///     Описания сущностей
        /// </summary>
        protected EntityDescriptor[] EntityDescriptors => _entityCollection.GetEntityDescriptors(GetType());

        /// <summary>
        ///     Включить режим ленивой загрузки
        /// </summary>
        protected virtual bool IsLazyLoading { get; set; }

        /// <inheritdoc/>
        public int Execute(string rawQuery)
        {
            return Database.ExecuteSqlRaw(rawQuery);
        }

        /// <inheritdoc/>
        public Task<int> ExecuteAsync(string rawQuery)
        {
            return Database.ExecuteSqlRawAsync(rawQuery);
        }

        /// <inheritdoc/>
        public T[] Query<T>(string rawQuery)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc/>
        public Task<T[]> QueryAsync<T>(string rawQuery)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (IsLazyLoading)
                optionsBuilder.UseLazyLoadingProxies();

            base.OnConfiguring(optionsBuilder);
        }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var entityDescriptor in EntityDescriptors)
            {
                dynamic map = entityDescriptor.Mapping;
                modelBuilder.ApplyConfiguration(map);
            }
        }
    }
}
