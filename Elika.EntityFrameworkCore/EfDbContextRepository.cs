﻿using Elika.Data;
using Elika.Data.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Elika.EntityFrameworkCore
{
    /// <summary>
    ///     Контекст базы данных с репозиторием
    /// </summary>
    public abstract class EfDbContextRepository : EfDbContext, IDbContextRepository
    {
        /// <inheritdoc />
        public EfDbContextRepository(EntityCollection entityCollection) : base(entityCollection)
        {
        }

        /// <inheritdoc />
        public EfDbContextRepository(EntityCollection entityCollection, DbContextOptions options) : base(entityCollection, options)
        {
        }

        /// <inheritdoc />
        public IDbRepositoryHandler<TDomain> GetHandler<TDomain>() where TDomain : class, IDomain
        {
            return new EntityDbRepositoryHandler<TDomain>(this);
        }
    }
}
