﻿using Elika.Data;
using Microsoft.EntityFrameworkCore;

namespace Elika.EntityFrameworkCore
{
    /// <summary>
    ///     Переопределение интерфейса конфигурации сущностей <see cref="IEntityTypeConfiguration{TEntity}"/>
    /// </summary>
    /// <typeparam name="TDbContext">Тип контекста</typeparam>
    /// <typeparam name="TEntity">Тип сущностей</typeparam>
    public interface IEntityTypeConfiguration<TDbContext, TEntity> : IEntityTypeConfiguration<TEntity>, IMapping<TDbContext, TEntity>
        where TDbContext : class, IDbContext 
        where TEntity : class, IDomain
    {
    }
}
