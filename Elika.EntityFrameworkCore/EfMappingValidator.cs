﻿using System;
using Elika.Data;
using Microsoft.EntityFrameworkCore;

namespace Elika.EntityFrameworkCore
{
    /// <summary>
    ///     Валидатор маппинга EF
    /// </summary>
    internal class EfMappingValidator : IMappingValidator
    {
        /// <summary>
        ///     Валидатор маппинга EF
        /// </summary>
        public static EfMappingValidator Validator { get; } = new EfMappingValidator();

        /// <inheritdoc />
        public void IsValid(Type mappingType)
        {
            if (!mappingType.IsParentOf(typeof(IEntityTypeConfiguration<>)))
                throw new ArgumentException();
        }
    }
}
